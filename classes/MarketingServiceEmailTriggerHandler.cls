/************************************************************
Description: This Class is the hanlder class for TaskCreationonInsert Trigger on Marketing Service Email Object.
             This is used to create a Task as soon as a Marketing Service Email recoird is inserted.
****************************************************************/

Public class MarketingServiceEmailTriggerHandler{
    Public static void TaskCreationonInsertMSE(List<Marketing_Service_Email__c> Triggerlist, Boolean isInsert){
    
     List<Task> taskList= new List<Task>();

    if(isInsert){
        List<Marketing_Service_Email__c> lmse= new List<Marketing_Service_Email__c>();
        lmse=[SELECT Contact__c,Contact__r.id,Contact__r.name,Contact__r.accountid,CreatedById,CreatedDate,Date_Sent__c,EmailID__c,Email_First_Paragraph__c,
                   Email_Header__c,Email_Link_HREF__c,Email_Link__c,Email_Name__c,Email__c,From_Address__c,From_Name__c,Id,IsDeleted,
                   JobID__c,LastActivityDate,LastModifiedById,LastModifiedDate,Lead__c,ListID__c,Name,OwnerId,Pre_Header__c,SubID__c,
                   Subject_Line__c,SystemModstamp FROM Marketing_Service_Email__c where Id  IN: Triggerlist];
                   
          for(Marketing_Service_Email__c mse : lmse){
                    Task taskSMS = new Task();
                    taskSMS.WhatId = mse.Contact__r.accountid;
                    taskSMS.status = 'Completed';
                    taskSMS.Subject = 'Email';
                    Map<String,Schema.RecordTypeInfo> taskRecordTypeInfoName  = GlobalUtil.getRecordTypeByName('Task');
                    taskSMS.RecordTypeId    = taskRecordTypeInfoName.get('Email').getrecordTypeId();            
                    taskSMS.WhoId = mse.Contact__c;
               /*     taskSMS.Description='Thanks again for Joining Belong. Now you&#39;re upda and running, we want to help you to get the most out of our Belong Service.'+
                                         mse.Email_Link__c;   */
                                         
                  /*  taskSMS.Description=mse.createddate+'</br>'+
                                        '<b>'+mse.Email_Header__c+'</b></br>'+'<p> Hi  '+mse.Contact__r.name+'</p></br>'
                                        +'<p>'+Label.CreateTaskDescription+'&nbsp&nbsp</p>'+
                                         '<a href="'+mse.Email_Link__c+'">'+'Further Details'+'</a>';   */
                    taskSMS.Description='<b>'+mse.Email_Header__c+'</b></br>'+
                                         '<a href="'+mse.Email_Link__c+'">'+'Further Details'+'</a>';                      
                    taskSMS.Notification_Date_Time__c= mse.CreatedDate;
                    taskSMS.OwnerId = mse.OwnerId;
                    taskSMS.Type='Email';
                    taskList.add(taskSMS);   
                    }
                    }
                    
                     if(!taskList.isEmpty()){
            insert taskList;    
        }
    
    }

}