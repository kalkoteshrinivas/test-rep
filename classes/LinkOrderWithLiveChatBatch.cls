/*
  @author  : Shrinivas Kalkote (shrinivas_kalkote@infosys.com)
  @created : 07/07/2016
  @Description : This batch class links survey provided by user with transcript object so. This will help in identifying survey against live agent transcript.
*/
global class LinkOrderWithLiveChatBatch implements Database.Batchable<Order>, Schedulable{

   global List<Order> start(Database.BatchableContext BC)
   {
      return [SELECT Id, LiveAgent_Session_Id__c, Live_Chat_Transcript__c FROM Order WHERE LiveAgent_Session_Id__c != NULL AND Live_Chat_Transcript__c = NULL];
   }

   global void execute(Database.BatchableContext BC, List<Order> lstOrder)
   {
     
     map<String, Order> mapChatKey = new map<String, Order>();
     
     for(Order oOrder : lstOrder)
     {
         mapChatKey.put(oOrder.LiveAgent_Session_Id__c, oOrder);
     }
     system.debug('lstOrder:: ' + lstOrder);
     for(LiveChatTranscript oLiveChatTranscript: [SELECT Id, Session_Id__c FROM LiveChatTranscript WHERE Session_Id__c IN: mapChatKey.keyset() /*AND CreatedDate = Today*/])
     {
         ((Order)mapChatKey.get(oLiveChatTranscript.Session_Id__c)).put('Live_Chat_Transcript__c', oLiveChatTranscript.Id);
     }

          system.debug('lstOrder:: ' + mapChatKey.values());
     update mapChatKey.values();
     
    }

   global void finish(Database.BatchableContext BC){
   }
   
   global void execute(SchedulableContext SC) {
      LinkOrderWithLiveChatBatch oLinkOrderWithLiveChatBatch = new LinkOrderWithLiveChatBatch();
      Database.executeBatch(oLinkOrderWithLiveChatBatch, 2000); 
   }
}