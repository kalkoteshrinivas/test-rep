public with sharing class EventsUtil 
{

    //keys
    private static final String REQUEST_STATUS = 'status';
    private static final String SEGMENT_STATUS = 'sub_status';
    private static final String SUBTYPE = 'order_subtype';
    private static final String APPT_TYPE = 'appointment_type';
    private static final String APPT_DATE = 'appointment_date';
    private static final String APPT_ID = 'appointment_id';
    private static final String APPT_START = 'appointment_start_time';
    private static final String APPT_END = 'appointment_end_time';
    private static final String SUBMITTED = 'submitted_date';
    private static final String REQUESTED = 'requested_date';
    private static final String COMPLETED = 'completed_date';
    private static final String SPID = 'sp_order_id';
    private static final String PROVIDERID = 'provider_ref';
    private static final String SERVICENO = 'service_number';
    private static final String ALARM = 'order_alarm';

    private static final String EVENT_SUBTYPE = 'subtype';

    //values
    private static final String COMPLETED_STATE = 'Completed';
    private static final String NBN_COMPLETE = 'RQC';
    private static final String ACTIVE_STATE = 'Active';
    private static final String IN_PROGRESS = 'In Progress';
    private static final String CONNECT_OUTSTANDING = 'Connect Outstanding';
    private static final String FIELD = 'SERVICE LOCATION';
    private static final String EXCHANGE = 'EXCHANGE';
    private static final String NBN_APPT = 'NBN_Confirmation';
    private static final String EVT_SUB_STATUS = 'STATUS';
    private static final String EVT_SUB_APPT = 'APPOINTMENT';
    private static final String PRODUCT_TYPE = 'product_type';

    private static final Map<String, String> REQUEST_STATUS_MEANING = new Map<String, String>
    {
        'RQIN' => 'Initial', 'RQT' => 'Transmitted', 'RQUTC' => 'Unable to complete',
        'RQU' => 'Under Assessment', 'RQH' => 'On Hold', 'RQA' => 'Active',
        'RQWD' => 'Withdrawn', 'RQC' => 'Completed', 'RQR' => 'Replaced',
        'RQJ' => 'Rejected', 'RQUTP' => 'Unable to Process'
    };

    private static String caseFields = 'Order_Health__c, Service__c, SLA_Missed__c, Unhealthy_Order_Status__c, Unhealthy_Status_Reason__c, '+
                'TELSTRA_REFERENCE__c, Customer_Octane_Number__c, Status, RecordTypeId, isClosed, contactId, CaseNumber,'+
                'Appointment_Date__c, Appointment_Start_Time__c, Appointment_End_Time__c, Request_Status__c, Segment_Status__c';


    public static void populateSMSTemplate(SFWrapperObject.Notification notifObj, List<SFWrapperObject.Notification> smsList,
                                            Map<String, sObject> accountMap, Map<String,String> messageAttributesMap, 
                                            Map<String,String> eventAttributesMap, List<Log__c> logs,List<String> messagingErrors)
    {
        
        try
        {
            String firstName = getName(notifObj.accountObj);
            notifObj.messageAttributes.add('firstname:'+ firstName);
            Case ifbotCase = getIFBOTCase(notifObj.accountObj, messageAttributesMap.get(SPID));

            if(messageAttributesMap.get(PRODUCT_TYPE) != 'NBN')
            {
                if(messageAttributesMap.get(REQUEST_STATUS) == COMPLETED_STATE && messageAttributesMap.get(COMPLETED) != null && 
                    isOrderStatusCompleted(ifbotCase))
                {
                    notifObj.smsTemplateCode = 'JOIN-ADSL';
                    smsList.add(notifObj);
                }
                else if(messageAttributesMap.get(APPT_TYPE) == FIELD && String.isNotBlank(messageAttributesMap.get(APPT_DATE)) &&
                    String.isNotBlank(messageAttributesMap.get(APPT_START)) && String.isNotBlank(messageAttributesMap.get(APPT_END)))
                {
                    /*if(messageAttributesMap.get(SEGMENT_STATUS) == IN_PROGRESS && messageAttributesMap.get(REQUEST_STATUS) == ACTIVE_STATE &&
                        eventAttributesMap.get(EVENT_SUBTYPE) == EVT_SUB_STATUS)
                    {
                        notifObj.smsTemplateCode = 'FIELD-STATUS-DSL';
                        smsList.add(notifObj);
                    }
                    else*/ if(messageAttributesMap.get(SEGMENT_STATUS) != CONNECT_OUTSTANDING && eventAttributesMap.get(EVENT_SUBTYPE) == EVT_SUB_APPT && 
                        isDSLAppointmentChanged(ifbotCase, messageAttributesMap)) 
                    {
                        notifObj.smsTemplateCode = 'FIELD-REAPPT-DSL';
                        smsList.add(notifObj);
                    }        
                       
                }

                else if(messageAttributesMap.get(APPT_TYPE) == EXCHANGE && String.isNotBlank(messageAttributesMap.get(APPT_DATE)))
                {
                    /*if(messageAttributesMap.get(SEGMENT_STATUS) == IN_PROGRESS && messageAttributesMap.get(REQUEST_STATUS) == ACTIVE_STATE &&
                        eventAttributesMap.get(EVENT_SUBTYPE) == EVT_SUB_STATUS)
                    {
                        notifObj.messageAttributes.add('plus2days:'+ addBusinessDays(messageAttributesMap.get(APPT_DATE), 2));
                        notifObj.smsTemplateCode = 'EXCG-STATUS-DSL';
                        smsList.add(notifObj);
                    }
                    else*/ if(messageAttributesMap.get(SEGMENT_STATUS) != CONNECT_OUTSTANDING && eventAttributesMap.get(EVENT_SUBTYPE) == EVT_SUB_APPT && 
                         isDSLAppointmentChanged(ifbotCase, messageAttributesMap))
                    {
                        notifObj.messageAttributes.add('plus2days:'+ addBusinessDays(messageAttributesMap.get(APPT_DATE), 2));
                        notifObj.smsTemplateCode = 'EXCG-REAPPT-DSL';
                        smsList.add(notifObj);  
                    }

                }                
            }
            else
            {
                if( messageAttributesMap.get(SEGMENT_STATUS) == NBN_COMPLETE && isNBNOrderStatusCompleted(ifbotCase) )
                {
                    notifObj.smsTemplateCode = 'JOIN-NBN';
                    smsList.add(notifObj);
                }
                else if(messageAttributesMap.get(APPT_TYPE) == NBN_APPT && String.isNotBlank(messageAttributesMap.get(APPT_DATE)))
                {
                    if(eventAttributesMap.get(EVENT_SUBTYPE) == EVT_SUB_APPT && isAppointmentChanged(ifbotCase, messageAttributesMap))
                    {
                        notifObj.smsTemplateCode = 'NBN-APPT';
                        smsList.add(notifObj);
                    }

                }
            }
        }
        catch(Exception e)
        {
            messagingErrors.add('Exception in sending SMS: ' + e.getMessage());
            logs.add(GlobalUtil.createLog('ERROR', e.getMessage().abbreviate(225), 'populateSMSTemplate',
                                                         JSON.Serialize(notifObj)));
        }
    }

    private static boolean isOrderStatusCompleted(Case caseObj)
    {
        if(caseObj != null && caseObj.Request_Status__c == COMPLETED_STATE )
        {
            return false;
        }
        return true;
    }

    private static boolean isNBNOrderStatusCompleted(Case caseObj)
    {
        if(caseObj != null && caseObj.Segment_Status__c == COMPLETED_STATE)
        {
            return false;
        }
        return true;
    }

    /*private static boolean isOrderSubStatusInProgress(Case caseObj)
    {
        if(caseObj != null && 
            caseObj.Segment_Status__c == IN_PROGRESS)
        {
            return false;
        }
        return true;
    }*/

    private static boolean isDSLAppointmentChanged(Case caseObj, Map<String,String> messageAttributesMap)
    {
        if(messageAttributesMap.get(APPT_TYPE) == FIELD && caseObj != null && ( caseObj.Appointment_Date__c == null || (caseObj.Appointment_Date__c == date.parse(messageAttributesMap.get(APPT_DATE))
            && (caseObj.Appointment_Start_Time__c == messageAttributesMap.get(APPT_START) &&
            caseObj.Appointment_End_Time__c == messageAttributesMap.get(APPT_END)))))
        {
            return false;
        }
        if(messageAttributesMap.get(APPT_TYPE) == EXCHANGE && caseObj != null && ( caseObj.Appointment_Date__c == null ||caseObj.Appointment_Date__c == date.parse(messageAttributesMap.get(APPT_DATE))))
        {
            return false;
        }       
        return true;
    }

    private static boolean isAppointmentChanged(Case caseObj, Map<String,String> messageAttributesMap)
    {
        if(caseObj != null && String.isNotBlank(messageAttributesMap.get(APPT_DATE)) &&
            caseObj.Appointment_Date__c == date.parse(messageAttributesMap.get(APPT_DATE)))
        {
            if(messageAttributesMap.get(APPT_TYPE) == EXCHANGE)
            {
                return false;
            }
            else if (caseObj.Appointment_Start_Time__c == messageAttributesMap.get(APPT_START) &&
            caseObj.Appointment_End_Time__c == messageAttributesMap.get(APPT_END))
            {
                return false;
            }
        }
        return true;
    }

    private static String getName(Account accObj)
    {
        for(Contact conObj : accObj.Contacts)
        {
            if(conObj.Contact_Role__c == 'Primary')
            {
                return conObj.firstname;
            }
        }

        return '';
    }

    public static String addBusinessDays(String apptDate, Integer noOfDays)
    {
        List<BusinessHours> bh = [SELECT Id FROM BusinessHours where name = 'Notification Hours'];

        Date apptDt = date.parse(apptDate);
        Datetime dt = datetime.newInstanceGmt(apptDt.year(), apptDt.month(),apptDt.day());
        long msToAdd = noOfDays * 24 * 60 * 60 * 1000;

        if(bh != null && !bh.isEmpty())
        {
            Datetime convertedDt = BusinessHours.addGmt(bh[0].Id, dt, msToAdd);
            return date.newinstance(convertedDt.year(), convertedDt.month(), convertedDt.day()).format();
        }

        return apptDt.addDays(noOfDays).format();

    }

    private static DateTime convertToDateTime(String dateTimeString)
    {
        List<String> convertedDateTimeValues = dateTimeString.replaceAll('[^0-9]',':').split(':');
        DateTime convertedDateTime  = DateTime.newInstance(Integer.valueOf(convertedDateTimeValues[0]), Integer.valueOf(convertedDateTimeValues[1]), 
                        Integer.valueOf(convertedDateTimeValues[2]), Integer.valueOf(convertedDateTimeValues[3]), Integer.valueOf(convertedDateTimeValues[4]), 
                        1);
        return  convertedDateTime;
    }

    public static void updateCaseFields(SFWrapperObject.Notification notifObj, List<Case> updatedCase, 
                                        Map<String,String> messageAttributesMap, List<Log__c> logs)
    {
        
        Case updatedCaseObj;

        try
        {            
            Case ifbotCase = getIFBOTCase(notifObj.accountObj, messageAttributesMap.get(SPID));
           
            if(ifbotCase != null)
            {
                updatedCaseObj = new Case(id = ifbotCase.Id);

                if(String.isNotBlank(messageAttributesMap.get(REQUEST_STATUS))) updatedCaseObj.Request_Status__c = messageAttributesMap.get(REQUEST_STATUS);
                if(String.isNotBlank(messageAttributesMap.get(SUBTYPE))) updatedCaseObj.Order_Subtype_Octane__c =  messageAttributesMap.get(SUBTYPE);
                if(String.isNotBlank(messageAttributesMap.get(APPT_TYPE))) updatedCaseObj.Appointment_Type__c  = messageAttributesMap.get(APPT_TYPE);
                if(String.isNotBlank(messageAttributesMap.get(APPT_DATE))) updatedCaseObj.Appointment_Date__c  = date.parse(messageAttributesMap.get(APPT_DATE));
                if(String.isNotBlank(messageAttributesMap.get(APPT_START))) updatedCaseObj.Appointment_Start_Time__c  =  messageAttributesMap.get(APPT_START);                
                if(String.isNotBlank(messageAttributesMap.get(APPT_END))) updatedCaseObj.Appointment_End_Time__c  =  messageAttributesMap.get(APPT_END);
                if(String.isNotBlank(messageAttributesMap.get(SUBMITTED))) updatedCaseObj.Telstra_Submitted_Date__c  = convertToDateTime(messageAttributesMap.get(SUBMITTED));
                if(String.isNotBlank(messageAttributesMap.get(REQUESTED))) updatedCaseObj.Customer_Requested_Date__c  =  messageAttributesMap.get(REQUESTED);
                if(String.isNotBlank(messageAttributesMap.get(COMPLETED))) updatedCaseObj.Completed_Date__c  = convertToDateTime(messageAttributesMap.get(COMPLETED));                
                if(String.isNotBlank(messageAttributesMap.get(PROVIDERID))) updatedCaseObj.Provider_Reference__c = messageAttributesMap.get(PROVIDERID);
                if(String.isNotBlank(messageAttributesMap.get(SERVICENO))) updatedCaseObj.Service__c = messageAttributesMap.get(SERVICENO);                
                
                if(messageAttributesMap.get(PRODUCT_TYPE) != 'NBN')
                {
                    if(String.isNotBlank(messageAttributesMap.get(SEGMENT_STATUS))) updatedCaseObj.Segment_Status__c = messageAttributesMap.get(SEGMENT_STATUS);
                }
                else
                {
                    if( messageAttributesMap.get(SEGMENT_STATUS) == NBN_COMPLETE && isNBNOrderStatusCompleted(ifbotCase) )
                    {
                        updatedCaseObj.Completed_Date__c  = datetime.now();
                    }
                    if(String.isNotBlank(messageAttributesMap.get(APPT_ID))) updatedCaseObj.Appointment_ID__c  =  messageAttributesMap.get(APPT_ID);
                    if(String.isNotBlank(messageAttributesMap.get(SEGMENT_STATUS))) updatedCaseObj.Segment_Status__c = REQUEST_STATUS_MEANING.get(messageAttributesMap.get(SEGMENT_STATUS));
                    if(String.isNotBlank(messageAttributesMap.get(SPID))) updatedCaseObj.sp_order_id__c  = messageAttributesMap.get(SPID);
                    if(String.isNotBlank(messageAttributesMap.get(ALARM))) updatedCaseObj.Telflow_Alarm__c = messageAttributesMap.get(ALARM);
                }

                updatedCase.add(updatedCaseObj);
            }
            else
            {
                logs.add(GlobalUtil.createLog('WARN', 'No Related IFBOT Case to Update', 'updateCaseFields',
                                                         JSON.Serialize(notifObj)));
            }
            
        }
        catch(Exception e)
        {
            logs.add(GlobalUtil.createLog('ERROR', e.getMessage().abbreviate(225), 'updateCaseFields',
                                                         JSON.Serialize(notifObj)));
        }

    }

    private static Case getIFBOTCase(Account accObj, String caseNo)
    {
        if(String.isNotBlank(caseNo))
        {
            return getCaseByNumber(caseNo);
        }
        else
        {
            Map<String,Schema.RecordTypeInfo> caseRecordTypeInfoName;
            caseRecordTypeInfoName  = GlobalUtil.getRecordTypeByName('Case');
            Id ifbotCaseId     = caseRecordTypeInfoName.get('IFBOT').getrecordTypeId();
            Id moveCaseId     = caseRecordTypeInfoName.get('Move Request').getrecordTypeId();

            if( !accObj.cases.isEmpty())
            {
                for(Case caseObj : accObj.cases) 
                {
                    if(caseObj.Request_Status__c != 'Completed' && (caseObj.RecordTypeId == ifbotCaseId || caseObj.RecordTypeId == moveCaseId))
                    {
                        return caseObj;
                    }
                }
            }
        }
        return null;
    }

    private static Case getCaseByNumber(String caseNo)
    {
        List<sObject> output = new List<sObject>();

        String query = 'Select '+ caseFields ;        
        query += ' FROM Case';
        query += ' WHERE CaseNumber =: caseNo';
        
        for (SObject o: Database.query(query))
        {
            output.add(o);
        }

        if(!output.isEmpty())
        {
            return ((Case)output[0]);
        }

        return null;
    }

    public static boolean validAppointmentYear(String apptDate, List<Log__c> logs )
    {
        boolean validAppointmentYear = false;
        Date apptDt;

        if(String.isNotBlank(apptDate))
        {
	        try
	        {
	            apptDt = date.parse( apptDate);
	        }
	        catch(Exception e)
	        {
	            logs.add(GlobalUtil.createLog('ERROR', e.getMessage().abbreviate(225), 'InvalidDate',
	                                                         apptDate));
	            return validAppointmentYear = false;
	        }
	        Date additional_one_year = date.today().addYears(1);
	        if(apptDt.year() <=  additional_one_year.year())
	        {
	            validAppointmentYear = true;
	        }
    	}
    	else
    	{
    		return validAppointmentYear = true;
    	}

        return validAppointmentYear;
    }
}